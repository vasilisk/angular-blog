import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiurl = "http://jsonplaceholder.typicode.com/posts";
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(this.apiurl);
  }

  getById(id: string) {
    return this.http.get(this.apiurl + '/' + id);
  }
}
