import { Component, OnInit } from '@angular/core';
import {PostService} from "../post.service";
import {UserService} from "../user.service";
import {CommentService} from "../comment.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  isFirstPage = true;
  isLastPage = false;
  page = 0;
  posts: any[] = [];
  users: any[] = [];
  comments: number[] = [];

  constructor(
    private postService: PostService,
    private userService: UserService,
    private commentService: CommentService
  ) { }

  ngOnInit(): void {
    this.loadPage(0);
  }

  loadPage(page: number) {
    this.page = page;
    this.isFirstPage = page == 0;
    this.postService.getAll().subscribe((data) => {
      // @ts-ignore
      let dataArr = Array.from(Object.keys(data), k=>data[k]);
      this.posts = dataArr.slice(10 * this.page, 10 * (this.page + 1));
      for (let post of this.posts) {
        this.loadUser(post.userId);
        this.loadCommentCount(post.id);
      }
      this.isLastPage = dataArr.length <= 10 * (this.page + 1);
    });
  }

  loadUser(id: string) {
    this.userService.getById(id).subscribe((data) => {
      // @ts-ignore
      this.users[id] = data;
    })
  }

  loadCommentCount(id: string) {
    this.commentService.getById(id).subscribe((data) => {
      // @ts-ignore
      let dataArr = Array.from(Object.keys(data), k=>data[k]);
      // @ts-ignore
      this.comments[id] = dataArr.length;
      console.log(dataArr.length);
    })
  }

}
