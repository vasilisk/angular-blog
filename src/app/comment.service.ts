import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private apiurl = "http://jsonplaceholder.typicode.com/comments?postId=";
  constructor(private http: HttpClient) { }

  getById(id: string) {
    return this.http.get(this.apiurl + id);
  }
}
