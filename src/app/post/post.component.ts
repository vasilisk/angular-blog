import { Component, DoCheck, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PostService} from "../post.service";
import {UserService} from "../user.service";
import {CommentService} from "../comment.service";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements DoCheck {

  post: any = {
    userId: null,
    id: null,
    title: "",
    body: ""
  };

  user: any = {
    name: '',
    email: ''
  };

  comments: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private userService: UserService,
    private commentService: CommentService
  ) { }

  ngDoCheck(): void {
    let id = this.route.snapshot.paramMap.get('id');
    if (id != null) {
      this.loadPost(id);
    }
  }

  loadPost(id: string) {
    this.postService.getById(id).subscribe((data) => {
      this.post = data;
      this.loadUser(this.post.userId);
      this.loadComments(this.post.id);
    });
  }

  loadUser(id: string) {
    this.userService.getById(id).subscribe((data) => {
      // @ts-ignore
      this.user = data;
    })
  }

  loadComments(id: string) {
    this.commentService.getById(id).subscribe((data) => {
      // @ts-ignore
      this.comments = Array.from(Object.keys(data), k=>data[k]);
    })
  }

}
